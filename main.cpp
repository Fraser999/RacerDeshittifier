#include <chrono>
#include <iostream>
#include <thread>
#include <vector>
#include "boost/filesystem/path.hpp"
#include "boost/filesystem/operations.hpp"

namespace bfs = boost::filesystem;

bool IsARacerJobby(const bfs::path& path) {
  try {
    return bfs::is_regular_file(path) && path.extension().empty() &&
        path.stem().string().size() == 9 && path.stem().string().substr(0, 3) == "tmp";
  } catch (...) {
    return false;
  }
}

std::vector<bfs::path> FindRacerJobbies(const bfs::path& path) {
  bfs::recursive_directory_iterator itr(path);
  bfs::recursive_directory_iterator end;
  std::vector<bfs::path> result;

  while (itr != end) {
    if (IsARacerJobby(*itr))
      result.push_back(*itr);

    if (bfs::is_directory(*itr) && bfs::is_symlink(*itr))
      itr.no_push();

    if ((*itr).path().filename().string() == ".git")
      itr.no_push();

    try {
      ++itr;
    } catch (...) {
      itr.no_push();
      try {
        ++itr;
      } catch (...) {
        return result;
      }
    }
  }

  return result;
}

void CleanUpJobbies(std::vector<bfs::path>&& jobbies) {
  for (const auto& jobby : jobbies) {
    try {
      bfs::remove(jobby);
    } catch (...) {
    }
    std::cout << "Deleted " << jobby << '\n';
  }
}

int main() {
  std::cout << "Cleaning up jobbies...\n";
  try {
    const bfs::path current_path(bfs::current_path());
    std::vector<bfs::path> jobbies(FindRacerJobbies(current_path));
    // Ensure Racer's finished shitting out all these files.
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    CleanUpJobbies(std::move(jobbies));
  } catch (...) {
    return -2;
  }

  std::cout << "All jobbies removed.  Press Enter to continue.\n";
  std::cin.ignore();
  return 0;
}
